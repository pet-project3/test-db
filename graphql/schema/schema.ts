import { buildSchema } from "graphql";

const graphqlSchema = buildSchema(`
type Companies{
    id: Int!
    companyCode: String!
    shortName: String!
    fullName: String!
    phone: Int!
    address:  String!
    provinceId: Int
    provinces: Provinces
}

type Provinces{
    id: Int!
    provinceCode: String!
    name: String!
    companies: [Companies]
}

type Query{
    #companiesPage(page: Int, 
    #itemPerPage: Int): [Companies]
    companies: [Companies]
    provinces: [Provinces]
    companiesPage (provinceId: Int!) : Companies
    provinceId (id: Int!): Provinces
}

type Mutation{
    updateCompany(id: Int, shortName: String, fullName: String): Companies
}

`);

export default graphqlSchema;
