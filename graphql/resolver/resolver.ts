import knex from "../config";

const graphqlResolvers = {
  Query: {
    companiesPage: (parent: any, args: any) => {
      // console.log("args", args);
      const { page = 1, itemPerPage = 3 } = args;

      const offset = itemPerPage * (page - 1);

      return knex("companies").offset(offset).limit(args.itemPerPage);
    },
    companies: () => {
      return knex("companies");
    },
    provinces: () => {
      return knex("region_provinces");
    },

    // args: tham số đc truyền vào từ schema
    provinceId: (parent: any, args: any) => {
      const sql = knex("region_provinces").where({
        id: args.id,
      });
      // console.log("=======", sql.toSQL());
      return sql;
    },
  },

  Companies: {
    provinces: (parent: any, args: any) => {
      // console.log("================", parent);
      const sql = knex("region_provinces")
        .where({
          id: parent.provinceId,
        })
        .first();
      // console.log("=====", sql.toSQL());
      return sql;
    },
  },
  Provinces: {
    companies: (parent: any, args: any) => {
      // console.log("================", parent);
      return knex("companies").where({
        provinceId: parent.id,
      });
    },
  },

  Mutation: {
    async updateCompany(parent: any, args: any) {
      const payload = await knex("companies")
        .where({
          id: args.id,
        })
        .update(
          {
            shortName: args.shortName,
            fullName: args.fullName,
          },
          [
            "id",
            "shortName",
            "fullName",
            "companyCode",
            "address",
            "provinceId",
          ]
        );

      return payload?.length > 0 ? payload[0] : null;
    },
  },
};

// const graphqlResolvers = {
//   async companies() {
//     try {
//       const sql = knex("companies")
// .limit(10)
//         .leftJoin(
//           "region_provinces",
//           `companies.provinceId`,
//           `region_provinces.id`
//         );
//       console.log("sql", sql.toString());
//       const data = await sql;

//       return data.map((item) => {
//         return {
//           ...item,
//           provinces: {
//             id: item.provinceId,
//             provinceCode: item.provinceCode,
//             name: item.name,
//           },
//         };
//       });
//     } catch (error) {
// throw new error
//     }
//   },

//   async provinces() {
//     try {
//       return knex("region_provinces");
//     } catch (error) {
// throw new error
//     }
//   },
// };

export default graphqlResolvers;
