import express from "express";
import { graphqlHTTP } from "express-graphql";
import graphqlResolvers from "./resolver/resolver";
import graphqlSchema from "./schema/schema";
import { makeExecutableSchema } from "@graphql-tools/schema";

const app = express();

app.use(
  "/graphql",
  graphqlHTTP({
    schema: makeExecutableSchema({
      typeDefs: graphqlSchema,
      resolvers: graphqlResolvers,
    }),
    // rootValue: graphqlResolvers,
    graphiql: true,
  })
);


app.listen(8000);
