import knex from "../van2";

export const getPage = async (filter: any) => {
  try {
    let { shortName, companyCode, phone } = filter;
    shortName = shortName ? shortName.trim() : "";
    let infor = knex("companies").select("*");
    if (shortName) {
      infor = infor.where("shortName", "like", `%${shortName}%`);
    }
    if (phone) {
      infor = infor.where("phone", "like", `${phone}`);
    }
    if (companyCode) {
      console.log('come in===s')
      infor = infor.where("companyCode", "like", `${companyCode}`);
    }
    console.log('sql===',infor.toString())
    return infor
  } catch (error) {
    return null;
  }
};
