const express = require("express");
const { getPage } = require("../services/search");

const router = express.Router();

router.get("/search", async (req: any, res: any) => {
  const { shortName, companyCode, phone } = req.query;
  getPage({ shortName, companyCode, phone })
    .then((data:any) => {
      console.log('data===',data)
      res.status(200).send(data);
    })
    .catch((err:any) => {
      console.log(err);
      res.status(500).send("can not find");
    });
});

export default router;
